from collections import OrderedDict
from flask import Flask, request, jsonify, json
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:password@localhost/exp_mgmt'
app.config["JSON_SORT_KEYS"] = False

db = SQLAlchemy(app)
class Expenses(db.Model):
    __tablename__ = 'expense'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.Unicode)
    email = db.Column('email', db.String(64))
    category = db.Column('category', db.String(64))
    description = db.Column('description', db.String(128))
    link = db.Column('link', db.String(255))
    estimated_costs = db.Column('estimated_costs', db.Integer)
    submit_date = db.Column('submit_date', db.String(20))
    status = db.Column('status', db.String(64))
    decision_date = db.Column('decision_date', db.String(20))

    def __init__(self, name, email, category, description, link, estimated_costs, submit_date, status, decision_date):
        self.name = name
        self.email = email
        self.category = category
        self.description = description
        self.link = link
        self.estimated_costs = estimated_costs
        self.submit_date = submit_date
        self.status = status
        self.decision_date = decision_date


@app.route("/v1/expenses", methods=['POST'])
def create_expense():
    expenses = json.loads(request.data)
    name = expenses['name']
    email = expenses['email']
    category = expenses['category']
    description = expenses['description']
    link = expenses['link']
    estimated_costs = expenses['estimated_costs']
    submit_date = expenses['submit_date']
    status = "pending"
    decision_date = ''

    exp = Expenses(name, email, category, description, link, estimated_costs, submit_date, status, decision_date)
    db.session.add(exp)
    db.session.commit()
    db.session.flush()
    expId = exp.id
    data = Expenses.query.get(expId)
    return jsonify(id=str(data.id),name=data.name,email=data.email,category=data.category, description=data.description, link=data.link, estimated_costs=str(data.estimated_costs), submit_date=data.submit_date, status=data.submit_date, decision_date = data.decision_date), 201


@app.route("/v1/expenses/<exp_id>", methods=['GET'])
def get_expenseById(exp_id):
    data = Expenses.query.filter_by(id = exp_id).first_or_404()
    
    expenses = jsonify(id=str(data.id),name=data.name,email=data.email,category=data.category, description=data.description, link=data.link, estimated_costs=str(data.estimated_costs), submit_date=data.submit_date, status=data.submit_date, decision_date = data.decision_date)
    return expenses, 200




@app.route("/v1/expenses/<exp_id>", methods=['PUT'])
def Update_expenseById(exp_id):
    e = Expenses.query.filter_by(id = exp_id).first_or_404()
    expenses = json.loads(request.data)
    e.estimated_costs = expenses['estimated_costs']
    db.session.commit()
    return '', 202



@app.route("/v1/expenses/<exp_id>", methods=['DELETE'])
def delete_expenseById(exp_id):
    e = Expenses.query.filter_by(id = exp_id).first_or_404()
    db.session.delete(e)
    db.session.commit()
    return '', 204

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)

