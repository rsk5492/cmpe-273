Posting data and looping through Id -->

"1"=>127.0.0.1:3000
"2"=>127.0.0.1:3000
"3"=>127.0.0.1:3000
"4"=>127.0.0.1:4000
"5"=>127.0.0.1:3000
"6"=>127.0.0.1:3000
"7"=>127.0.0.1:5000
"8"=>127.0.0.1:3000
"9"=>127.0.0.1:3000
"10"=>127.0.0.1:3000


Getting the data by GET from DB -->

"0"=>127.0.0.1:3000
{
"id" : "1",
"name" : "Foo 1",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"1"=>127.0.0.1:3000
{
"id" : "2",
"name" : "Foo 2",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"2"=>127.0.0.1:3000
{
"id" : "3",
"name" : "Foo 3",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"3"=>127.0.0.1:4000
{
"id" : "4",
"name" : "Foo 4",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"4"=>127.0.0.1:3000
{
"id" : "5",
"name" : "Foo 5",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"5"=>127.0.0.1:3000
{
"id" : "6",
"name" : "Foo 6",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"6"=>127.0.0.1:5000
{
"id" : "7",
"name" : "Foo 7",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"7"=>127.0.0.1:3000
{
"id" : "8",
"name" : "Foo 8",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"8"=>127.0.0.1:3000
{
"id" : "9",
"name" : "Foo 9",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
"9"=>127.0.0.1:3000
{
"id" : "10",
"name" : "Foo 10",
"email" : "foo1@bar.com",
"category" : "office supplies",                
"description" : "iPad for office use",
"link" : "http://www.apple.com/shop/buy-ipad/ipad-pro",
"estimated_costs" : "700",                
"submit_date" : "12-10-2016",
"status" : "pending",
"decision_date" : ""
}
